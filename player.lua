function CreatePlayer()
	-- Prepare textures
	local textures = { player_engine_on = love.graphics.newImage( 'images/spaceship_power_on.png' ), player_engine_off = love.graphics.newImage( 'images/spaceship_power_off.png' ) }

	-- Initalize the player
	local player = { 
		isActive = false, 
		position = { x = 0, y = 0 }, 
		rotation = 0.0, 
		texture = textures.player_engine_off, 
		score = 0, 
		lifes = 3, 
		speed = 150,
		trigger_a = false, 
		size = { width = 32, height = 32 },
		fired = false
	}

	-- Update the player with the delta time
	function player:update( dt )
		if player.isActive then
			if player.lifes == 0 then
				destroyPlayer()
			end

			local is_moving = false

			-- movement
			if player:keyboardMovement( dt ) then 
				is_moving = true
			elseif player:jostick( dt ) then
				is_moving = true
			end

			if is_moving then
				player.texture = textures.player_engine_on
			else
				player.texture = textures.player_engine_off
			end

			ComeBackToScreen( player )
			player:calculatePlayerRotation( dt )

			if player.trigger_a then 
				generateBullet( player )
				player.trigger_a = false
			end
		end
	end

	function player:jostick( dt )
		local isMoving = false

		if love.joystick.getNumJoysticks() > 0 then
			local movement = love.joystick.getAxis( 1, 2 )
			local rotation = love.joystick.getAxis( 1, 5 )

			if movement < -0.2 then
				player:moveForward( dt )
				isMoving = true
			end

			if rotation < -0.2 then 
				player:rotateLeft( dt )
			elseif rotation > 0.2 then
				player:rotateRight( dt )
			end

			if love.joystick.isDown( 1, 6 ) then
				if not fired then 
					generateBullet( player )
					fired = true
				end
			end

			if not love.joystick.isDown( 1, 6 ) then
				if fired then 
					fired = false
				end
			end

			if love.joystick.isDown( 1, 7 ) then 
				os.exit()
			end
		end

		return isMoving
	end

	function player:keyboardMovement( dt )
		local isMoving = false

		if love.keyboard.isDown( "up" ) or love.keyboard.isDown( "w" ) then
			player:moveForward( dt )
			isMoving = true
		end

		if love.keyboard.isDown( "left" ) or love.keyboard.isDown( "a" ) then
			player:rotateLeft( dt ) 
		end

		if love.keyboard.isDown( "right" ) or love.keyboard.isDown( "d" ) then
			player:rotateRight( dt )
		end

		return isMoving
	end

	function player:moveForward( dt )
		player.position.x = player.position.x + ( player.speed * dt ) * math.cos ( player.rotation - math.pi / 2 )
		player.position.y = player.position.y + ( player.speed * dt ) * math.sin ( player.rotation - math.pi / 2 )
	end

	function player:rotateLeft( dt )
		player.rotation = ( player.rotation - dt )
	end

	function player:rotateRight( dt )
		player.rotation = ( player.rotation + dt )
	end

	-- Draw the player
	function player:draw()
		if player.isActive then 
			love.graphics.draw( 
				player.texture, 
				player.position.x, 
				player.position.y, 
				player.rotation, 
				0.5, 
				0.5, 
				player.texture:getWidth() / 2, 
				player.texture:getHeight() / 2 
			)
		end
	end

	-- Caluclate the score at the end of the game
	function player:finish()
		player.score = player.score * player.lifes
	end

	-- Increase the score by a given value
	function player:increasePlayerScore( value )
		player.score = player.score + value
	end

	-- Decrease the player lifes after a collision
	function player:hitPlayer()
		player.lifes = player.lifes - 1
	end

	-- Calulates the player rotation for draw and fire direction
	function player:calculatePlayerRotation( dt )
		local player_center_x = player.position.x + player.size.width / 2
		local player_center_y = player.position.y + player.size.height / 2
		local mouse_x, mouse_y = love.mouse.getPosition()

		pv = { player_center_x, player_center_y }
		mv = { mouse_x, mouse_y }

		-- player.rotation = toMouseAngle( mv , pv )
	end

	return player
end