function MoveObject( obj, dt )
	obj.position.x = obj.position.x + ( obj.speed * dt ) * math.cos( obj.rotation ) -- (currentPosition.X + speed * Math.Cos(MathHelper.ToRadians(Direction)))
	obj.position.y = obj.position.y + ( obj.speed * dt ) * math.sin( obj.rotation ) -- (currentPosition.Y + speed * Math.Sin(MathHelper.ToRadians(Direction)))
	ComeBackToScreen( obj )
end

function ComeBackToScreen( obj )
	if obj.position.x < 0 then
		obj.position.x = love.graphics.getWidth()
	end

	if obj.position.x > love.graphics.getWidth() then
		obj.position.x = 0
	end

	if obj.position.y < 0 then
		obj.position.y = love.graphics.getHeight()
	end

	if obj.position.y > love.graphics.getHeight() then
		obj.position.y = 0
	end
end

--[[ Awesome calculation of an angle between two vectors ]]--
function toMouseAngle( x, y )
	local v = math.atan( ( y[2] - x[2] ) / ( y[1] - x[1] ) )
	local vdeg = v * 180 / math.pi

	if x[1] < y[1] then
		v = ( vdeg + 270 ) * math.pi / 180
	else 
		v = ( vdeg + 90 ) * math.pi / 180
	end
	
	return v
end

function round( val, decimal )
	if decimal then
		return math.floor( ( val * 10^decimal ) + 0.5 ) / ( 10^decimal )
	else
		return math.floor( val + 0.5 )
	end
end

function handleTimer( timer, dt )
	if timer.elapsedTime > timer.deadline then
		timer.isElapsed = true
	else
		timer.elapsedTime = timer.elapsedTime + dt
	end
end

function checkRectCollision( obj1, obj2 )
	local rect1 = { top = obj1.position.y, bottom = obj1.position.y + obj1.size.height, left = obj1.position.x, right = obj1.position.x + obj1.size.width }
	local rect2 = { top = obj2.position.y, bottom = obj2.position.y + obj2.size.height, left = obj2.position.x, right = obj2.position.x + obj2.size.width }

	if ( rect1.top < rect2.bottom and rect1.bottom > rect2.top ) and ( rect1.right > rect2.left and rect1.left < rect2.right ) then
		return true
	else
		return false
	end
end

function cleanTable( tbl )
	for i = 1, table.getn( tbl ) do
		if tbl[i] then
			if tbl[i].isActive == false then
				table.remove( tbl, i )
				i = i - 1
			end
		end
	end
end