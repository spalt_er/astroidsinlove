function CreateBullet( obj )
	-- Prepare Textures
	local textures = { bullet = love.graphics.newImage( 'images/bullet.png' ) }

	-- Initialize
	local bullet = { 
		position = { x = obj.position.x , y = obj.position.y }, 
		speed = 400, 
		texture = textures.bullet, 
		isActive = true, 
		rotation = obj.rotation - math.pi / 2, -- for some reason the direction was 90 deg to the right
		timer = { isElapsed = false, elapsedTime = 0, deadline = 2 },
		size = { width = 16, height = 16 }
	}

	-- Update the bullet with the delta time
	function bullet:update( dt )
		if bullet.isActive then 
			MoveObject( bullet, dt )
			handleTimer( bullet.timer , dt )

			if bullet.timer.isElapsed then
				bullet.isActive = false
			end
		end
	end

	-- Draw the bullet
	function bullet:draw()
		if bullet.isActive then 
			love.graphics.draw( 
				bullet.texture, 
				bullet.position.x, 
				bullet.position.y, 
				bullet.rotation + math.pi / 2, 
				1, 
				1, 
				bullet.size.width, 
				bullet.size.height 
			)
		end
	end

	return bullet;
end