function CreateAstroid( level )
	local textures = { astriod = love.graphics.newImage( 'images/astroid.png' ) }
	local astroid = {
		isActive = true, 
		speed = 60, 
		position = { 
			x = math.random( 1, love.graphics.getWidth() - textures.astriod:getWidth() ), 
			y = math.random( 1, love.graphics.getHeight() - textures.astriod:getHeight() ) 
		}, 
		rotation = math.random( 0, 359 ), 
		texture = textures.astriod,
		timer = { isElapsed = false, elapsedTime = 0, deadline = 500 },
		level = level,
		size = { width = 32, height = 32 }
	}

	function astroid:update( dt )
		if astroid.isActive then 
			MoveObject( astroid, dt )
			handleTimer( astroid.timer , dt )
			
			if astroid.timer.isElapsed then
				astroid.isActive = false
			end
		end
	end

	function astroid:draw()
		if astroid.isActive then 
			local size = { x = astroid.texture:getWidth() / astroid.level, y = astroid.texture:getHeight() / astroid.level }
			love.graphics.draw( 
				astroid.texture, 
				astroid.position.x, 
				astroid.position.y, 
				astroid.rotation, 
				0.25 / ( astroid.level / 2 ), 
				0.25 / ( astroid.level / 2 ), 
				size.x / 2, 
				size.y / 2 
			)
		end
	end

	return astroid;
end