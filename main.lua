require "bloom"
require "player"
require "bullet"
require "astroid"
require "helper"

local game = { title = "AstroidsinLove", isActive = false, completed = false, wave = 1 }
local player = CreatePlayer()
local astroids = { noAstroids = 5, level = 2 }
local bullets = { maxBulletCount = 3 }
local graphics = { resolution = { width = 1330, height = 720 },	effects = { bloom } }

-- Startup
function love.load()
	--Record the screen dimensions
	-- love.graphics.setMode(0, 0, false, false)
	-- graphics.resolution.width = love.graphics.getWidth()
	-- graphics.resolution.height = love.graphics.getHeight()

	-- game
	math.randomseed(love.timer.getMicroTime())
    love.graphics.setMode(graphics.resolution.width, graphics.resolution.height, false, true, 4)
    love.graphics.setCaption(game.title)

    -- effects
	graphics.effects.bloom = CreateBloomEffect(graphics.resolution.width/4, graphics.resolution.height/4)

	-- player
	player.position.x = love.graphics.getWidth() / 2
	player.position.y = love.graphics.getHeight() / 2

	-- astroids
	for i = 0, astroids.noAstroids do
		GenerateAstroid( 1 )
	end

	-- finish
	player.isActive = true;
	game.isActive = true;
end

function loadLevel()
	player.name = "Player 1"
	player.position = { x = 100, y = 50 }
end
-- End startup

-- Input stuff
function love.mousereleased(x, y, button)
	player.trigger_a = true
end

function love.mousepressed(x, y, button)

end

function love.keypressed(key, unicode)

end

function love.keyreleased(key)
	if key == "escape" then
		os.exit()
	end

	if key == " " or key == "lctrl" or key == "rctrl" then 
		generateBullet( player )
	end
end
-- End Input

-- Drawing stuff
function drawAstroids()
	for i = 1, table.getn( astroids ) do 
		astroids[i]:draw()
	end
end

function drawBullets()
	for i = 1, table.getn( bullets ) do 
		bullets[i]:draw()
	end
end

function love.draw()
	-- Game
	love.graphics.setColor( 112, 146, 190, 255 )
	love.graphics.setBackgroundColor( 11, 26, 47 )

    if game.isActive then
		drawContent()
		drawEffects()
	end

	if game.completed then
		drawFinishInfo()
	else
		drawGameInfo()
	end

	-- drawDebugInfo()
end

function drawContent() 
	drawBullets()
	drawAstroids()
	player:draw()
end

function drawDebugInfo( )
	love.graphics.print( game.title, 50, 50 )
	love.graphics.print( string.format( "No. astroids: %q", table.getn( astroids ) ) , 50, 60 )
	love.graphics.print( string.format( "No. Bullets: %q", table.getn( bullets ) ), 50, 70 )
	love.graphics.print( string.format( "Lifes: %q Score: %q", player.lifes, player.score ), 50, 80 )
	love.graphics.print( string.format( "Player Rotation: %q", player.rotation ), 50, 90 )
	love.graphics.print( string.format( "Axes: %q", love.joystick.getNumAxes( 1 ) ), 50, 100 )

	local line = 110
	for i = 1, love.joystick.getNumAxes( 1 ) do
		love.graphics.print( string.format( "Rot on Axis %q: %q", i, love.joystick.getAxis( 1, i ) ), 50, line )
		line = line + 10
	end
	for i = 1, love.joystick.getNumButtons( 1 ) do
		local res = love.joystick.isDown( 1, i )
		if res then val = "true" else val = "false" end
		love.graphics.print( string.format( "Button %q: %q", i, val ), 50, line )
		line = line + 10
	end
end

function drawEffects()
	-- Bloom
	graphics.effects.bloom:predraw()
	graphics.effects.bloom:enabledrawtobloom()		
	drawContent()
	graphics.effects.bloom:postdraw() 
end

function drawGameInfo()
	local score = string.format( "Score: %q", player.score )
	local wave = string.format( "Wave: %q", game.wave )
	love.graphics.print( string.format( "Life: %q", player.lifes ), 30, 20 )
	love.graphics.print( score, love.graphics.getWidth() - string.len( score ) * 6 - 30, 20 )
	love.graphics.print( wave, ( love.graphics.getWidth() / 2 ) - string.len( wave ) * 6 / 2, 20 )
end

function drawFinishInfo()
	if game.fail then
		love.graphics.print( 
			string.format( "Game over!" ), 
			love.graphics.getWidth() / 2 - 60, 
			love.graphics.getHeight() / 2 - 50
		)
	else
		love.graphics.print( 
			string.format( "Congratulations! You beat all %q waves! Score: %q", game.wave, player.score ), 
			love.graphics.getWidth() / 2 - 170, 
			love.graphics.getHeight() / 2 - 50
		)
	end
end
-- End draw stuff

-- Updates on all the stuff
function love.update(dt)
	if ( game.isActive ) then 
		update( dt )
	end
end

function update( dt )
	player:update( dt )
	updateastroids( dt )
	updateBullets( dt )

	cleanUp()

	updateGame( dt )
end

function updateastroids( dt )
	for i = 1, table.getn( astroids ) do
		astroids[i]:update( dt )

		if checkRectCollision( astroids[i], player ) then
			player:hitPlayer()
			astroids[i].isActive = false
		end
	end
end

function updateBullets( dt )
	for i = 1, table.getn( bullets ) do
		bullets[i]:update( dt )
		for n = 1, table.getn( astroids ) do
			if checkRectCollision( bullets[i], astroids[n] ) then
				astroids[n].isActive = false
				bullets[i].isActive = false
				player:increasePlayerScore( 100 )
				splitAstroid( astroids[n] )
			end
		end

		if checkRectCollision( bullets[i], player ) and bullets[i].timer.elapsedTime > 1 then
			player:hitPlayer()
		end
	end
end

function updateGame( dt )
	if game.isActive then
		checkWave()

		if game.wave == 5 then
			finish()
		end
	end
end
-- End update stuff

-- misc
function cleanUp()
	cleanTable( astroids )
	cleanTable( bullets )
end
--end misc

-- Game rules
function generateBullet( obj )
	if table.getn( bullets ) < bullets.maxBulletCount then 
		table.insert( bullets, CreateBullet( obj ) )
	end
end

function checkWave()
	if table.getn( astroids ) == 0 then
		nextWave()
		player:increasePlayerScore( 500 )
	end
end

function nextWave()
	game.wave = game.wave + 1
	for i = 0, astroids.noAstroids * game.wave do
		GenerateAstroid( 1 )
	end
end

function finish()
	game.isActive = false
	game.completed = true
	player:finish()
end

function destroyPlayer()
	game.fail = true
	finish()
end

function splitAstroid( parent )
	if parent.level <= astroids.level then
		local level = parent.level * astroids.level

		for i = 1, level do
			GenerateAstriodAt( level, parent.position )
		end
	end
end

function GenerateAstroid( level )
	local astroid = CreateAstroid( level )

	if checkRectCollision( player, astroid ) then
		GenerateAstroid( 1 )
	else
		table.insert( astroids, astroid )
	end
end

function GenerateAstriodAt( level, pos )
	local astroid = CreateAstroid( level )
	astroid.position.x = pos.x
	astroid.position.y = pos.y
	
	table.insert( astroids, astroid )
end
-- End game rules